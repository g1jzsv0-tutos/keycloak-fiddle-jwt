package org.example.keycloak.fiddle.jwt.service;

public record Greeting(long id, String content) {
}
