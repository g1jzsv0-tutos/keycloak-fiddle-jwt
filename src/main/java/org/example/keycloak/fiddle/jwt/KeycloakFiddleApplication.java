package org.example.keycloak.fiddle.jwt;

import org.example.keycloak.fiddle.jwt.controller.GreetingController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
public class KeycloakFiddleApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakFiddleApplication.class, args);
    }

}
